## group_vars

Groups are in the format tag_Role_, tag_Location_, and tag_Env_.  This is to align with the ec2 dynamic inventory method of grouping.  It's assumed that instances created in ec2 (eg using terraform) will have the relevant tags associated (Role: app, Env: dev, etc)
