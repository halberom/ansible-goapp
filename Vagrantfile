# -*- mode: ruby -*-
# vi: set ft=ruby :

boxes = [
  { "name"=>"app01", "playbook"=>"plays/cfg/app.yml", "ip"=>"192.168.23.11" },
  { "name"=>"app02", "playbook"=>"plays/cfg/app.yml", "ip"=>"192.168.23.12" },
  { "name"=>"web01", "playbook"=>"plays/cfg/web.yml", "ip"=>"192.168.23.10" },
]

groups = {
  "tag_Role_web"               => ["web01"],
  "tag_Role_app"               => ["app01", "app02"],
  "tag_Location_vagrant:children" => [
    "tag_Role_web",
    "tag_Role_app"
  ],
  "tag_Env_dev:children"          => ["tag_Location_vagrant"],
  "dev_vagrant:children"          => ["tag_Env_dev", "tag_Location_vagrant"]
}

Vagrant.configure(2) do |config|
  ### Define options for all VMs ###
  # Using vagrant-cachier improves performance if you run repeated yum/apt updates
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
    config.cache.auto_detect = false
    config.cache.enable :apt
  end

  # By default, Vagrant 1.7+ automatically inserts a different
  # insecure keypair for each new VM created. The easiest way
  # to use the same keypair for all the machines is to disable
  # this feature and rely on the legacy insecure key.
  config.ssh.insert_key = false

  config.vm.provider :virtualbox do |vb|
    vb.customize [
      "modifyvm", :id,
      "--memory", "512",
      "--cpus", "2",
      "--ioapic", "on",
      "--natdnsproxy1", "on",
    ]
  end

  config.vm.box = "ubuntu/trusty64"

  boxes.each do |opts|
    config.vm.define opts["name"] do |node|

      if opts.has_key?("ip")
        node.vm.network "private_network", ip: opts["ip"]
      else
        node.vm.network "private_network", type: "dhcp"
      end

      if Vagrant.has_plugin?("vagrant-hosts")
        node.vm.provision :hosts, :sync_hosts => true
      end

      node.vm.host_name = opts["name"]

      node.vm.provision "ansible" do |ansible|
        ansible.playbook = opts["playbook"]
        ansible.groups = groups
        ansible.limit = "all"
        if ENV['EXTRA_VARS']
          ansible.extra_vars = JSON.parse(ENV['EXTRA_VARS'])
        end
      end
    end
  end
end

