# goapp

This repository contains an ansible layout for configuring an Nginx web server and several App servers running a Golang application.  It uses several external ansible roles.

## Requirements

- ansible v1.9 or newer
- for testing
-- Vagrant v1.7.4 or newer
-- Virtualbox v5.0.8 or newer

## Setup

Source the roles from galaxy/github

    ansible-galaxy install -r requirements.yml

## Configure

At present, the repository is only configured for a vagrant setup.  Other providers (AWS etc) can be added as required.

Using an inventory of your choice, configure the servers

    ansible-playbook plays/cfg/app.yml -i inv/...
    ansible-playbook plays/cfg/web.yml -i inv/...

or in one line

    ansible-playbook plays/cfg/site.yml -i inv/...

## Deploy

Using an inventory of your choice, force a deploy to the app servers

    ansible-playbook plays/ops/deploy.yml -i inv/...

## Testing

This repository contains a Vagrantfile for testing in Virtualbox.

Install the following vagrant plugins.

    # This ensures the instances can talk to each other
    vagrant plugin install vagrant-hosts
    # This caches apt packages locally
    vagrant plugin install vagrant-cachier

To launch all servers, and provision them automatically, run

    vagrant up

You can then browse to http://192.168.23.10

## TODO

- Fix it so the servers added to Nginx aren't hardcoded as app01 and app02.

Initial work has been done in Vagrant only, further design and implementation work is required for cloud style environments - e.g. DNS, security etc.

## License

BSD/MIT

## Author

This repo was created in 2016 by [Gerard Lynch](http://halberom.com/)
